# Cab Data API

This project defines a set of Restful APIs of handling NY cab data. The source code repository is in [BitBucket](https://bitbucket.org/marvin_liu/cab-data-api/src/master/).

The API provides a way to query how many trips a particular cab (medallion) has made given a particular pickup date. You can
query one or more cabs at one time for a given date.

The API also provides a way to clear cache for a specific cab in a specific date.

Refer to [API Reference Section](#Api Reference) for more information.

## Running

### Start it
It is recommended to run it with docker contains. The Application and its dependencies are defined in `docker-compose.yaml`.

Please note the first run will take longer time as it needs to download docker images and load the data into MySQL.

```shell
make start-docker

make stop-docker
```

Alternatively you can run the app locally by `make run` however you will need to setup a Redis and a MySQL server in your local environment and make sure they are configured correctly.

### Play with it

You can play with the APIs with a GUI tool like `Postman` or from CLI using `cURL`. Here are some `cURL` examples:

#### Get trip counts of multiple cabs on a given day

```shell
curl -X GET \
  http://localhost:8080/api/v1/trip-counts \
  -H 'Content-Type: application/json' \
  -d '{
	"medallions": ["00FD1D146C1899CEDB738490659CAD30", "F1B42A266FB027C5CD5AB19FDD549E0F", "5943FA32855CEA9401FF1774F28AA42C", "71B9C3F3EE5EFB81CA05E9B90C91C88F"],
	"date": "2013-12-03",
	"noCache": true
  }'
```

The response body looks like:
```json
[
    {
        "medallion": "71B9C3F3EE5EFB81CA05E9B90C91C88F",
        "count": 5
    },
    {
        "medallion": "5943FA32855CEA9401FF1774F28AA42C",
        "count": 17
    },
    {
        "medallion": "00FD1D146C1899CEDB738490659CAD30",
        "count": 19
    },
    {
        "medallion": "F1B42A266FB027C5CD5AB19FDD549E0F",
        "count": 6
    }
]
```

#### Bust a cache

```bash
curl -X DELETE http://localhost:8080/api/v1/admin/cache/5943FA32855CEA9401FF1774F28AA42C/2013-12-03
```

The response would be `204 No Content` if it is successful.

## Developing

### Code structure

```
├── cmd                            # Application entry file
├── db                             # Files to setup a pre-populated MySQL docker container
├── lib
│   ├── cache                      # Module that handles redis cache
│   ├── controller                 # Routes and controllers layer
│   ├── database                   # Module that handles mysql operations
│   ├── model                      # Models
│   ├── service                    # Services layer
│   └── wiring                     # Wiring and setting up
└── swagger.yaml
```

### Dependencies

See `go.mod` for dependencies.

### Prerequisites

1. Go 1.13.1
2. Docker
3. Redis
4. MySQL

### Building

The project follows Go official convention in terms of development tooling. See `Makefile` for available commands and they are self-explanatory.

## Configuration

The project assumes reasonable defaults for all configurations. If you need to override the config, use `.env` file.

See `lib/wiring/config.go` for more information.

## Tests

Due to the time limit I did not write unit tests for all files however I tried to cover three different scenarios by following tests:

- HTTP req/resp tests: see `lib/controller/cache_test.go`
- DB mock tests: see `lib/database/mysql_test.go`
- Service layer tests: see `lib/service/cache_test.go` and `lib/service/counts_test.go`

To run the tests:
```bash
make test
```

## Style guide

Styles are enforced by `go fmt` tool.

```bash
make fmt
```

## Api Reference

Details can be found in `swagger.yaml`.

TL;DR:

- GET /api/v1/trip-counts
I chose to put the queries in request body to make it easier to support multiple cabs, which is inspired by ElasticSearch API.

A sample request body looks like
```json
{
	"medallions": ["00FD1D146C1899CEDB738490659CAD30", "F1B42A266FB027C5CD5AB19FDD549E0F", "5943FA32855CEA9401FF1774F28AA42C", "71B9C3F3EE5EFB81CA05E9B90C91C88F"],
	"date": "2013-12-03",
	"noCache": true
}
```
Note that when `noCache` is `true`, the server will skip cache and return fresh data only.

- DELETE /api/v1/admin/cache/{medallion}/{date}
It deletes the cache data specified by `medallion` and `date`.

## Production Ready
1. Add more unit tests
2. Add E2E tests
3. Enable tracing
4. Add healthcheck endpoint
5. Collect metrics
