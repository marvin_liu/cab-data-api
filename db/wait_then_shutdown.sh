#!/usr/bin/env bash
while [ ! -f /tmp/server_can_shutdown ]
do
  sleep 2
done
kill $(pidof mysqld)