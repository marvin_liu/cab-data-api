# stage 1) Build
FROM golang:1.13.1-alpine AS build-go

RUN apk add --no-cache \
            bash \
            curl \
            git \
            make && \
    rm -rf /var/cache/apk/*

WORKDIR /go/src/bitbucket.org/marvin_liu/cab-data-api

# Add the rest of the source and build
COPY . /go/src/bitbucket.org/marvin_liu/cab-data-api
RUN make build

# stage 2) Run
FROM golang:1.13.1-alpine

WORKDIR /app

COPY --from=build-go /go/src/bitbucket.org/marvin_liu/cab-data-api/cab-data-api /app/

ENTRYPOINT ["./cab-data-api"]
