package cache

import (
	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"
)

// TripCountsCache defines the cab data cache
type TripCountsCache struct {
	redisPool *redis.Pool
}

// NewTripCountsCache returns new TripCountsCache
func NewTripCountsCache(redisPool *redis.Pool) TripCountsCache {
	return TripCountsCache{redisPool: redisPool}
}

// Get retrieves count cache by medallion and date
func (c TripCountsCache) Get(logger *zap.Logger, medallion string, date string) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()

	count, err := redis.Int64(conn.Do("GET", medallion+"_"+date))
	if err != nil {
		logger.Error("Unable to get count from redis", zap.Error(err), zap.String("medallion", medallion), zap.String("date", date))
		return count, err
	}

	logger.Debug("Got cache from redis", zap.String("medallion", medallion), zap.String("date", date), zap.Int64("count", count))
	return count, nil
}

// Set sets a new count cache using medallion and date as key
func (c TripCountsCache) Set(logger *zap.Logger, medallion string, date string, count int64) error {
	conn := c.redisPool.Get()
	defer conn.Close()

	_, err := conn.Do("SET", medallion+"_"+date, count)
	if err != nil {
		logger.Error("Unable to save count to cache", zap.Error(err), zap.String("medallion", medallion), zap.String("date", date), zap.Int64("count", count))
		return err
	}

	logger.Debug("Set cache to redis", zap.String("medallion", medallion), zap.String("date", date), zap.Int64("count", count))
	return nil
}

// Delete removes a cache using medallion and date as key
func (c TripCountsCache) Delete(logger *zap.Logger, medallion string, date string) error {
	conn := c.redisPool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", medallion+"_"+date)
	if err != nil {
		logger.Error("Unable to delete cache", zap.Error(err), zap.String("medallion", medallion), zap.String("date", date))
		return err
	}

	logger.Debug("Delete cache from redis", zap.String("medallion", medallion), zap.String("date", date))
	return nil
}
