package controller

import (
	"net/http"

	"go.uber.org/zap"
)

// Route defines a route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes contains all routes
type Routes []Route

// RoutesConfig defines routes for trip counts APIs
func RoutesConfig(logger *zap.Logger, tripCountsService TripCountsService, cacheService CacheService) Routes {
	return Routes{
		Route{
			"TripCounts",
			"GET",
			"/trip-counts",
			tripCountsHandler(logger, tripCountsService),
		},
		Route{
			"Cache",
			"DELETE",
			"/admin/cache/{medallion}/{date}",
			cacheHandler(logger, cacheService),
		},
	}
}
