package controller

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/marvin_liu/cab-data-api/lib/model"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

var logger, _ = zap.NewDevelopment()

type cacheServiceMockState int

func mockLoggingMW(inner http.Handler) http.Handler {
	return inner
}

const (
	cacheServiceSuccess cacheServiceMockState = iota
	cacheServiceFailure
)

type cacheServiceMock struct {
	state cacheServiceMockState
}

type tripCountsServiceMock struct{}

func (s *cacheServiceMock) BustCache(logger *zap.Logger, medallion string, date string) error {
	switch s.state {
	case cacheServiceSuccess:
		return nil
	case cacheServiceFailure:
		return errors.New("Internal Server Error")
	default:
		panic(fmt.Sprintf("Unknown state: %v", s.state))
	}
}

func (s *tripCountsServiceMock) TripCounts(logger *zap.Logger, medallions []string, date string, noCache bool) ([]model.TripCount, error) {
	return nil, errors.New("Internal Server Error")
}

func TestCacheHandler(t *testing.T) {
	testTable := []struct {
		Name      string
		Body      string
		MockState cacheServiceMockState
		Path      string
		Status    int
	}{
		{
			Name:      "Ok Delete",
			MockState: cacheServiceSuccess,
			Path:      "/api/v1/admin/cache/test-medallion/2019-09-02",
			Status:    204,
		},
		{
			Name:      "Unknown error",
			MockState: cacheServiceFailure,
			Path:      "/api/v1/admin/cache/test-medallion/2019-09-02",
			Status:    500,
		},
		{
			Name:      "Not found",
			MockState: cacheServiceSuccess,
			Path:      "/api/v1/admin/cache//2019-09-02",
			Status:    404,
		},
		{
			Name:      "Invalid Value: date",
			MockState: cacheServiceSuccess,
			Path:      "/api/v1/admin/cache/test-medallion/2019-09",
			Status:    400,
		},
	}

	for _, d := range testTable {
		t.Run(d.Name, func(t *testing.T) {
			params := RouteParams{
				Logger:            logger,
				Middleware:        Middleware{Logging: mockLoggingMW},
				TripCountsService: &tripCountsServiceMock{},
				CacheService:      &cacheServiceMock{state: d.MockState},
			}

			r := NewRouter(params)
			ts := httptest.NewServer(r)
			defer ts.Close()

			req, err := http.NewRequest("DELETE", ts.URL+d.Path, nil)
			require.NoError(t, err, "Error setting request")

			res, err := http.DefaultClient.Do(req)
			require.NoError(t, err, "Error setting request")

			assert.Equal(t, d.Status, res.StatusCode, "Status code")
		})
	}
}
