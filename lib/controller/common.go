package controller

import (
	"encoding/json"
	"fmt"
	"mime"
	"net/http"
)

const (
	headerContentType = "Content-Type"
	contentTypeJSON   = "application/json"
)

type jsonError struct {
	Message string `json:"message"`
}

func (error jsonError) Error() string {
	return error.Message
}

// GeneralError represents a single generic error with an explanatory message
func generalError(message string) error {
	return jsonError{
		Message: message,
	}
}

func requireJSONContentType(contentTypeHeader string) error {
	mediatype, _, err := mime.ParseMediaType(contentTypeHeader)
	if err != nil {
		return fmt.Errorf("Invalid ContentType [%s]", contentTypeHeader)
	}

	if mediatype != contentTypeJSON {
		return fmt.Errorf("Unsupported ContentType [%s]. Must be [%s]", mediatype, contentTypeJSON)
	}

	return nil
}

func writeClientError(w http.ResponseWriter, encoder *json.Encoder, message string) {
	w.WriteHeader(http.StatusBadRequest)
	encoder.Encode(generalError(message))
}

func writeServerError(w http.ResponseWriter, encoder *json.Encoder) {
	code := http.StatusInternalServerError
	w.WriteHeader(code)
	encoder.Encode(generalError(http.StatusText(code)))
}

func writeResponseNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

func writeResponseOK(w http.ResponseWriter, encoder *json.Encoder, response interface{}) {
	w.WriteHeader(http.StatusOK)
	encoder.Encode(response)
}
