package controller

import (
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

// RouteParams contains parameters needed for initializing a router and all its http handlers
type RouteParams struct {
	Logger            *zap.Logger
	Middleware        Middleware
	TripCountsService TripCountsService
	CacheService      CacheService
}

// Middleware defines the middlewares to apply to relevant routes which may be more complex to create
type Middleware struct {
	Logging func(http.Handler) http.Handler
}

var root = func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`
		<!DOCTYPE html>
		<html>
			<body>
			<h1>Index Page</h1>
			</body>
		</html>
	`))
}

// NewRouter creates a new HTTP router, registering the required routes and any middleware as appropriate
func NewRouter(p RouteParams) http.Handler {
	r := mux.NewRouter().StrictSlash(true)

	baseMW := chainMiddleware(p.Middleware.Logging)

	r.Handle("/", http.HandlerFunc(root)).Methods("GET")
	api := r.PathPrefix("/api/v1").Subrouter()

	for _, route := range RoutesConfig(p.Logger, p.TripCountsService, p.CacheService) {
		api.
			Handle(route.Pattern, baseMW(http.HandlerFunc(route.HandlerFunc))).
			Methods(route.Method)
	}

	return r
}

type middleware func(http.Handler) http.Handler

func chainMiddleware(first middleware, chain ...middleware) middleware {
	if len(chain) == 0 {
		return first
	}

	return func(h http.Handler) http.Handler {
		return first(chainMiddleware(chain[0], chain[1:]...)(h))
	}
}
