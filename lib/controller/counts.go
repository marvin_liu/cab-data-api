package controller

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"

	"bitbucket.org/marvin_liu/cab-data-api/lib/model"
)

// TripCountsService defines service layer interface for trip counts
type TripCountsService interface {
	TripCounts(logger *zap.Logger, medallions []string, date string, noCache bool) ([]model.TripCount, error)
}

// tripCountHandler returns an httpHandler for route GET /trip-counts
func tripCountsHandler(logger *zap.Logger, tripCountsService TripCountsService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enc := json.NewEncoder(w)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		contentTypeHeader := r.Header.Get(headerContentType)
		if err := requireJSONContentType(contentTypeHeader); err != nil {
			writeClientError(w, enc, "Invalid Content Type")
			return
		}

		tr, err := loadTripCountsRequest(logger, w, r, enc)
		if err != nil {
			logger.Error("Unable to read request from request body", zap.Error(err))
			writeClientError(w, enc, "Unable to read request from request body")
			return
		}

		if validated := tr.Validate(); !validated {
			logger.Error("Invalid request", zap.Any("request", tr))
			writeClientError(w, enc, "Invalid request")
			return
		}

		output, err := tripCountsService.TripCounts(logger, tr.Medallions, tr.Date, tr.NoCache)
		if err != nil {
			logger.Error("Failed to serve trip counts request", zap.Error(err), zap.Any("request", tr))
			writeServerError(w, enc)
			return
		}

		writeResponseOK(w, enc, output)
	}
}

func loadTripCountsRequest(logger *zap.Logger, w http.ResponseWriter, r *http.Request, enc *json.Encoder) (*model.TripCountsRequest, error) {
	dec := json.NewDecoder(r.Body)
	var tcr model.TripCountsRequest
	err := dec.Decode(&tcr)
	if err != nil {
		return nil, err
	}

	return &tcr, nil
}
