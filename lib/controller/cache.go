package controller

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/marvin_liu/cab-data-api/lib/model"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

// CacheService defines service layer interface for cache
type CacheService interface {
	BustCache(logger *zap.Logger, medallion string, date string) error
}

// cacheHandler returns an httpHandler for route DELETE /admin/cache/{medallion}/{date}
func cacheHandler(logger *zap.Logger, cacheService CacheService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enc := json.NewEncoder(w)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		params := mux.Vars(r)
		medallion := params["medallion"]
		date := params["date"]

		cr := model.CacheRequest{
			Medallion: medallion,
			Date:      date,
		}

		if validated := cr.Validate(); !validated {
			logger.Error("Invalid request", zap.Any("request", cr))
			writeClientError(w, enc, "Invalid request")
			return
		}

		err := cacheService.BustCache(logger, cr.Medallion, cr.Date)
		if err != nil {
			logger.Error("Failed to serve cache bust request", zap.Error(err), zap.Any("request", cr))
			writeServerError(w, enc)
			return
		}

		writeResponseNoContent(w)
	}
}
