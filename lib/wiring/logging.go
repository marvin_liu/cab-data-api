package wiring

import (
	"net/http"
	"time"

	"go.uber.org/zap"
)

// LoggingResponseWriter will encapsulate a standard ResponseWritter with a copy of its statusCode
type LoggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

// ResponseWriterWrapper is supposed to capture statusCode from ResponseWriter
func ResponseWriterWrapper(w http.ResponseWriter) *LoggingResponseWriter {
	return &LoggingResponseWriter{w, http.StatusOK}
}

// WriteHeader is a surcharge of the ResponseWriter method
func (lrw *LoggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

// NewLoggingMiddleware returns the logging middleware
func NewLoggingMiddleware(logger *zap.Logger) func(http.Handler) http.Handler {
	return func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			wrapper := ResponseWriterWrapper(w)
			inner.ServeHTTP(wrapper, r)
			// Example: 127.0.0.1 - Marvin [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 286.219µs
			logger.Info("Request/Response details",
				zap.Duration("Duration", time.Since(start)),
				zap.String("Remote address", r.RemoteAddr),
				zap.String("Method", r.Method),
				zap.String("Request URI", r.RequestURI),
				zap.Int("Status code", wrapper.statusCode),
			)
		})
	}
}
