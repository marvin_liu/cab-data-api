package wiring

// Config holds app configuration values
type Config struct {
	DevMode bool   `envconfig:"default=true"`
	Host    string `envconfig:"optional"`
	HTTP    struct {
		Port int `envconfig:"default=8080"`
	}
	Redis struct {
		MaxActive int    `envconfig:"default=12000"`
		MaxIdle   int    `envconfig:"default=80"`
		Protocol  string `envconfig:"default=tcp"`
		URL       string `envconfig:"default=redis:6379"`
	}
	DBURL string `envconfig:"default=cab_data_api:cab_data_api@tcp(mysql:3306)/db"`
}
