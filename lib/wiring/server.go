package wiring

import (
	"database/sql"
	"fmt"
	"net/http"

	// Required by MySQL connection
	_ "github.com/go-sql-driver/mysql"
	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"

	"bitbucket.org/marvin_liu/cab-data-api/lib/cache"
	"bitbucket.org/marvin_liu/cab-data-api/lib/controller"
	"bitbucket.org/marvin_liu/cab-data-api/lib/database"
	"bitbucket.org/marvin_liu/cab-data-api/lib/service"
)

// Server represents a configured server instance that can be started
type Server interface {
	Run()
	Close()
	http.Handler
}

type server struct {
	http.Handler
	config *Config
	db     *sql.DB
	redis  *redis.Pool
	logger *zap.Logger
}

// Initialize creates a Server from the config which is ready to execute
func Initialize(cfg *Config, logger *zap.Logger) Server {
	// Set up Redis
	pool := newPool(logger, redisConfig{
		MaxActive: cfg.Redis.MaxActive,
		MaxIdle:   cfg.Redis.MaxIdle,
		Protocol:  cfg.Redis.Protocol,
		URL:       cfg.Redis.URL,
	})

	// Set up MySQL
	db, err := sql.Open("mysql", cfg.DBURL)
	if err != nil {
		panic(err)
	}

	tripCountsCache := cache.NewTripCountsCache(pool)
	tripCountsDB := database.NewTripCountsDB(db)
	tripCountsService := service.NewTripCountsService(logger, tripCountsCache, tripCountsCache, tripCountsDB)
	cacheService := service.NewCacheService(logger, tripCountsCache)

	r := controller.NewRouter(controller.RouteParams{
		Logger: logger,
		Middleware: controller.Middleware{
			Logging: NewLoggingMiddleware(logger),
		},
		TripCountsService: tripCountsService,
		CacheService:      cacheService,
	})

	return &server{
		Handler: r,
		db:      db,
		redis:   pool,
		logger:  logger,
		config:  cfg,
	}
}

// Run starts the server so that it can begin handling requests
func (s *server) Run() {
	listenAddr := fmt.Sprintf("%s:%d", s.config.Host, s.config.HTTP.Port)
	s.logger.Info("Starting server", zap.String("listenAddress", listenAddr))

	if err := http.ListenAndServe(listenAddr, s); err != nil {
		s.logger.Fatal("Unable to start server.", zap.Error(err))
	}
}

func (s *server) Close() {
	s.logger.Info("Stopping server")
	defer s.db.Close()
	defer s.redis.Close()
}
