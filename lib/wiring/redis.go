package wiring

import (
	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"
)

type redisConfig struct {
	MaxActive int
	MaxIdle   int
	Protocol  string
	URL       string
}

func newPool(logger *zap.Logger, c redisConfig) *redis.Pool {
	return &redis.Pool{
		MaxIdle:   c.MaxIdle,
		MaxActive: c.MaxActive,

		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial(c.Protocol, c.URL)
			if err != nil {
				logger.Fatal("Failed to set up redis connection", zap.Error(err))
			}
			return c, nil
		},
	}
}
