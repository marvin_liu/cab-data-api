package model

import (
	"time"
)

// TripCountsRequest models requests of getting trip counts
type TripCountsRequest struct {
	Medallions []string `json:"medallions,omitempty"`
	Date       string   `json:"date,omitempty"`
	NoCache    bool     `json:"noCache,omitempty"`
}

// TripCount contains the count of trips of a can and its medallion
type TripCount struct {
	Medallion string `json:"medallion"`
	Count     int64  `json:"count"`
}

// TripCountsResponse models responses of getting trip counts
type TripCountsResponse []TripCount

// Validate validates a contact
func (r TripCountsRequest) Validate() bool {
	return validateMedallions(r.Medallions) && validateDate((r.Date))
}

// CacheRequest models requests of busting cache
type CacheRequest struct {
	Medallion string `json:"medallion,omitempty"`
	Date      string `json:"date,omitempty"`
}

// Validate validates a contact
func (r CacheRequest) Validate() bool {
	return len(r.Medallion) > 0 && validateDate((r.Date))
}

func validateMedallions(medallions []string) bool {
	if len(medallions) < 1 {
		return false
	}

	for _, m := range medallions {
		if len(m) < 1 {
			return false
		}
	}

	return true
}

func validateDate(date string) bool {
	_, err := time.Parse("2006-01-02", date)
	if err != nil {
		return false
	}

	return true
}
