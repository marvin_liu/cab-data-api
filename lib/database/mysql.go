package database

import (
	"database/sql"

	"go.uber.org/zap"
)

// TripCountsDB defines the cab data DB
type TripCountsDB struct {
	conn *sql.DB
}

// NewTripCountsDB returns new TripCountsDB
func NewTripCountsDB(conn *sql.DB) TripCountsDB {
	return TripCountsDB{conn: conn}
}

// Read queries count from DB
func (c TripCountsDB) Read(logger *zap.Logger, medallion string, date string) (int64, error) {
	startTime := date + " 00:00:00"
	endTime := date + " 23:59:59"

	logger.Debug("Calculated start and end time", zap.String("startTime", startTime), zap.String("endTime", endTime))

	row := c.conn.QueryRow("SELECT Count(medallion) FROM cab_trip_data WHERE medallion=? AND pickup_datetime >= ? AND pickup_datetime <= ?", medallion, startTime, endTime)
	var count int64
	err := row.Scan(&count)
	if err != nil {
		logger.Error("Error happened when querying MySQL", zap.Error(err), zap.String("medallion", medallion), zap.String("date", date))
		return 0, err
	}

	return count, nil
}
