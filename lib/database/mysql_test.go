package database

import (
	"errors"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

var mockDBError = errors.New("DB Error")

var logger, _ = zap.NewDevelopment()

func TestReadOnSuccess(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	rows := sqlmock.NewRows([]string{"Count(medallion)"}).
		AddRow(145)

	mock.ExpectQuery("SELECT").WithArgs("test-medallion", "2012-02-13 00:00:00", "2012-02-13 23:59:59").WillReturnRows(rows)

	conn := NewTripCountsDB(db)
	count, err := conn.Read(logger, "test-medallion", "2012-02-13")
	assert.Equal(t, int64(145), count, "%s: Count", "TestReadOnSuccess")
	require.NoError(t, err, "%s: Error", "TestReadOnSuccess")

	err = mock.ExpectationsWereMet()
	require.NoError(t, err, "%s: Error", "TestReadOnSuccess")
}

func TestReadOnFailure(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	mock.ExpectQuery("SELECT").WithArgs("test-medallion", "2012-02-13 00:00:00", "2012-02-13 23:59:59").WillReturnError(mockDBError)

	conn := NewTripCountsDB(db)
	_, err = conn.Read(logger, "test-medallion", "2012-02-13")
	assert.Equal(t, mockDBError, err, "%s: Error", "TestReadOnFailure")

	err = mock.ExpectationsWereMet()
	require.NoError(t, err, "%s: Error", "TestReadOnFailure")
}
