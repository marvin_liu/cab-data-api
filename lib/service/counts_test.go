package service

import (
	"errors"
	"fmt"
	"testing"

	"bitbucket.org/marvin_liu/cab-data-api/lib/model"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

type cacheGetterMockState int
type cacheSetterMockState int
type dbReaderMockState int

const (
	cacheGetterSuccess cacheGetterMockState = iota
	cacheGetterPartialFailure
	cacheGetterFailure
	cacheGetterOther
	cacheSetterSuccess cacheSetterMockState = iota
	cacheSetterFailure
	cacheSetterOther
	dbReaderSuccess dbReaderMockState = iota
	dbReaderFailure
	dbReaderOther

	testMedallionA string = "test-medallion-a"
	testMedallionB string = "test-medallion-b"

	mockCacheResultA int64 = 23
	mockCacheResultB int64 = 34
	mockDBResultA    int64 = 14
	mockDBResultB    int64 = 9
)

var mockOtherError = errors.New("Unexpected Error")
var mockDBError = errors.New("DB Error")

var logger, _ = zap.NewDevelopment()

type cacheGetterMock struct {
	state cacheGetterMockState
}

type cacheSetterMock struct {
	state cacheSetterMockState
}

type dbReaderMock struct {
	state dbReaderMockState
}

func (c *cacheGetterMock) Get(logger *zap.Logger, medallion string, date string) (int64, error) {
	switch c.state {
	case cacheGetterSuccess:
		switch medallion {
		case testMedallionA:
			return mockCacheResultA, nil
		case testMedallionB:
			return mockCacheResultB, nil
		default:
			panic(fmt.Sprintf("Unknown medallion: %v", medallion))
		}
	case cacheGetterPartialFailure:
		switch medallion {
		case testMedallionA:
			return 0, mockOtherError
		case testMedallionB:
			return mockCacheResultB, nil
		default:
			panic(fmt.Sprintf("Unknown medallion: %v", medallion))
		}
	case cacheGetterFailure:
		return 0, mockOtherError
	default:
		panic(fmt.Sprintf("Unknown state: %v", c.state))
	}
}

func (c *cacheSetterMock) Set(logger *zap.Logger, medallion string, date string, count int64) error {
	switch c.state {
	case cacheSetterSuccess:
		return nil
	case cacheSetterFailure:
		return mockOtherError
	default:
		panic(fmt.Sprintf("Unknown state: %v", c.state))
	}
}

func (a *dbReaderMock) Read(logger *zap.Logger, medallion string, date string) (int64, error) {
	switch a.state {
	case dbReaderSuccess:
		switch medallion {
		case testMedallionA:
			return mockDBResultA, nil
		case testMedallionB:
			return mockDBResultB, nil
		default:
			panic(fmt.Sprintf("Unknown medallion: %v", medallion))
		}
	case dbReaderFailure:
		return 0, mockDBError
	default:
		panic(fmt.Sprintf("Unknown state: %v", a.state))
	}
}

func TestTripCounts(t *testing.T) {
	t.Parallel()

	cacheGetter := &cacheGetterMock{}
	cacheSetter := &cacheSetterMock{}
	dbReader := &dbReaderMock{}

	testTable := []struct {
		Name                 string
		CacheGetterMockState cacheGetterMockState
		CacheSetterMockState cacheSetterMockState
		DBReaderMockState    dbReaderMockState
		ExpectedResult       []model.TripCount
		Error                error
	}{
		{
			Name:                 "Success - both from cache",
			CacheGetterMockState: cacheGetterSuccess,
			CacheSetterMockState: cacheSetterOther,
			DBReaderMockState:    dbReaderOther,
			ExpectedResult:       []model.TripCount{{Medallion: testMedallionA, Count: 23}, {Medallion: testMedallionB, Count: 34}},
		},
		{
			Name:                 "Success - both from DB",
			CacheGetterMockState: cacheGetterFailure,
			CacheSetterMockState: cacheSetterSuccess,
			DBReaderMockState:    dbReaderSuccess,
			ExpectedResult:       []model.TripCount{{Medallion: testMedallionA, Count: 14}, {Medallion: testMedallionB, Count: 9}},
		},
		{
			Name:                 "Success - A from DB, B from cache",
			CacheGetterMockState: cacheGetterPartialFailure,
			CacheSetterMockState: cacheSetterSuccess,
			DBReaderMockState:    dbReaderSuccess,
			ExpectedResult:       []model.TripCount{{Medallion: testMedallionA, Count: 14}, {Medallion: testMedallionB, Count: 34}},
		},
		{
			Name:                 "Failure - both errors",
			CacheGetterMockState: cacheGetterFailure,
			CacheSetterMockState: cacheSetterOther,
			DBReaderMockState:    dbReaderFailure,
			Error:                errors.New("Failed to get trip counts"),
		},
		{
			Name:                 "Failure - one error",
			CacheGetterMockState: cacheGetterPartialFailure,
			CacheSetterMockState: cacheSetterOther,
			DBReaderMockState:    dbReaderFailure,
			Error:                errors.New("Failed to get trip counts"),
		},
	}

	for _, tr := range testTable {
		t.Run(tr.Name, func(t *testing.T) {
			cacheGetter.state = tr.CacheGetterMockState
			cacheSetter.state = tr.CacheSetterMockState
			dbReader.state = tr.DBReaderMockState

			s := NewTripCountsService(logger, cacheGetter, cacheSetter, dbReader)

			res, err := s.TripCounts(logger, []string{testMedallionA, testMedallionB}, "2019-02-12", false)
			if tr.Error == nil {
				require.NoError(t, err, "%s: Error", tr.Name)
				assert.ElementsMatch(t, tr.ExpectedResult, res, "%s: Counts service", tr.Name)
			} else {
				assert.Equal(t, tr.Error, err, "%s: Error", tr.Name)
			}
		})
	}
}
