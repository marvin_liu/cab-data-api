package service

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

type cacheBusterMockState int

const (
	cacheBusterSuccess cacheBusterMockState = iota
	cacheBusterFailure

	testMedallion string = "test-medallion"
)

type cacheBusterMock struct {
	state cacheBusterMockState
}

func (c *cacheBusterMock) Delete(logger *zap.Logger, medallion string, date string) error {
	switch c.state {
	case cacheBusterSuccess:
		return nil
	case cacheBusterFailure:
		return mockOtherError
	default:
		panic(fmt.Sprintf("Unknown state: %v", c.state))
	}
}

func TestBustCache(t *testing.T) {
	t.Parallel()

	cacheBuster := &cacheBusterMock{}

	testTable := []struct {
		Name                 string
		CacheBusterMockState cacheBusterMockState
		Error                error
	}{
		{
			Name:                 "Success",
			CacheBusterMockState: cacheBusterSuccess,
		},
		{
			Name:                 "Failure",
			CacheBusterMockState: cacheBusterFailure,
			Error:                mockOtherError,
		},
	}

	for _, tr := range testTable {
		t.Run(tr.Name, func(t *testing.T) {
			cacheBuster.state = tr.CacheBusterMockState

			s := NewCacheService(logger, cacheBuster)

			err := s.BustCache(logger, testMedallion, "2019-02-12")
			if tr.Error == nil {
				require.NoError(t, err, "%s: Error", tr.Name)
			} else {
				assert.Equal(t, tr.Error, err, "%s: Error", tr.Name)
			}
		})
	}
}
