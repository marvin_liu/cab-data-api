package service

import (
	"errors"
	"sync"
	"sync/atomic"

	"go.uber.org/zap"

	"bitbucket.org/marvin_liu/cab-data-api/lib/model"
)

// TripCountsCacheGetter defines a Trip Counts cache getter
type TripCountsCacheGetter interface {
	Get(logger *zap.Logger, medallion string, date string) (int64, error)
}

// TripCountsCacheSetter defines a Trip Counts cache setter
type TripCountsCacheSetter interface {
	Set(logger *zap.Logger, medallion string, date string, count int64) error
}

// TripCountsDBReader defines a Trip Counts DB reader
type TripCountsDBReader interface {
	Read(logger *zap.Logger, medallion string, date string) (int64, error)
}

// TripCountsService defines services for handling Trip Counts
type TripCountsService struct {
	logger      *zap.Logger
	cacheGetter TripCountsCacheGetter
	cacheSetter TripCountsCacheSetter
	dbReader    TripCountsDBReader
}

// NewTripCountsService returns an instance of TripCountsService
func NewTripCountsService(
	logger *zap.Logger, cacheGetter TripCountsCacheGetter, cacheSetter TripCountsCacheSetter,
	dbReader TripCountsDBReader,
) *TripCountsService {
	return &TripCountsService{
		logger:      logger,
		cacheGetter: cacheGetter,
		cacheSetter: cacheSetter,
		dbReader:    dbReader,
	}
}

// TripCounts returns trip counts by given medallions and date
func (s *TripCountsService) TripCounts(logger *zap.Logger, medallions []string, date string, noCache bool) ([]model.TripCount, error) {
	logger.Debug("Start querying trip counts", zap.Any("medallions", medallions), zap.String("date", date), zap.Bool("noCache", noCache))

	resultChan := make(chan model.TripCount, len(medallions))
	var errCount uint64
	wg := &sync.WaitGroup{}

	if noCache {
		for _, medallion := range medallions {
			wg.Add(1)
			go freshTripCount(logger, wg, resultChan, &errCount, s, medallion, date)
		}
	} else {
		for _, medallion := range medallions {
			wg.Add(1)
			go tripCount(logger, wg, resultChan, &errCount, s, medallion, date)
		}
	}

	go func() {
		wg.Wait()
		close(resultChan)
	}()

	var results []model.TripCount
	for result := range resultChan {
		results = append(results, result)
	}

	if errCount > 0 {
		logger.Error("Errors happened during queries", zap.Any("errCount", errCount))
		return nil, errors.New("Failed to get trip counts")
	}

	logger.Debug("Get results from queries", zap.Any("results", results))

	return results, nil
}

func tripCount(logger *zap.Logger, wg *sync.WaitGroup, resultChan chan model.TripCount, errCount *uint64, s *TripCountsService, medallion string, date string) {
	defer wg.Done()

	count, err := s.cacheGetter.Get(logger, medallion, date)
	if err == nil {
		logger.Debug("Redis hit", zap.String("medallion", medallion), zap.String("date", date))
		resultChan <- model.TripCount{
			Medallion: medallion,
			Count:     count,
		}

		return
	}

	count, err = s.dbReader.Read(logger, medallion, date)
	if err != nil {
		logger.Error("Failed to read count from DB", zap.String("medallion", medallion), zap.String("date", date), zap.Error(err))
		atomic.AddUint64(errCount, 1)

		return
	}

	go s.cacheSetter.Set(logger, medallion, date, count)

	logger.Debug("Got count from DB", zap.String("medallion", medallion), zap.String("date", date), zap.Int64("count", count))
	resultChan <- model.TripCount{
		Medallion: medallion,
		Count:     count,
	}

	logger.Debug("Wrote result to channel", zap.String("medallion", medallion), zap.Int64("count", count))

	return
}

func freshTripCount(logger *zap.Logger, wg *sync.WaitGroup, resultChan chan model.TripCount, errCount *uint64, s *TripCountsService, medallion string, date string) {
	defer wg.Done()

	count, err := s.dbReader.Read(logger, medallion, date)
	if err != nil {
		logger.Error("Failed to read count from DB", zap.String("medallion", medallion), zap.String("date", date), zap.Error(err))
		atomic.AddUint64(errCount, 1)

		return
	}

	go s.cacheSetter.Set(logger, medallion, date, count)

	resultChan <- model.TripCount{
		Medallion: medallion,
		Count:     count,
	}

	return
}
