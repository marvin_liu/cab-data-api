package service

import (
	"go.uber.org/zap"
)

// TripCountsCacheBuster defines a Trip Counts cache buster
type TripCountsCacheBuster interface {
	Delete(logger *zap.Logger, medallion string, date string) error
}

// CacheService defines services for handling Cache
type CacheService struct {
	logger      *zap.Logger
	cacheBuster TripCountsCacheBuster
}

// NewCacheService returns an instance of CacheService
func NewCacheService(
	logger *zap.Logger, cacheBuster TripCountsCacheBuster,
) *CacheService {
	return &CacheService{
		logger:      logger,
		cacheBuster: cacheBuster,
	}
}

// BustCache clears a cache
func (s *CacheService) BustCache(logger *zap.Logger, medallion string, date string) error {
	logger.Debug("Start busting cache", zap.Any("medallion", medallion), zap.String("date", date))

	return s.cacheBuster.Delete(logger, medallion, date)
}
