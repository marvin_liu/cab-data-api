package main

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/vrischmann/envconfig"
	"go.uber.org/zap"

	"bitbucket.org/marvin_liu/cab-data-api/lib/wiring"
)

var (
	cfg    wiring.Config
	logger *zap.Logger
)

func main() {
	s := wiring.Initialize(&cfg, logger)
	defer s.Close()
	s.Run()
}

func init() {
	// Load the environment variables if a file named .env is placed in the root folder.
	// It will not affect the app in production environment.
	err := godotenv.Load()
	if err == nil {
		log.Print("Loaded .env file")
	}

	// Initialize Config by reading from environment variables
	err = envconfig.Init(&cfg)
	if err != nil {
		log.Fatal("Error loading config", zap.Error(err))
	}

	// Initialize zap logger
	if cfg.DevMode {
		logger, err = zap.NewDevelopment()
	} else {
		logger, err = zap.NewProduction()
	}
	if err != nil {
		log.Fatal("Error initializing logger", zap.Error(err))
	}
}
