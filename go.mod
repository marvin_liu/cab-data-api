module bitbucket.org/marvin_liu/cab-data-api

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/vrischmann/envconfig v1.2.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0
	google.golang.org/appengine v1.6.5 // indirect
)
