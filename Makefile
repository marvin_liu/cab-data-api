APP_NAME := cab-data-api

# Default target
build: clean depend fmt go-build

clean:
	rm -f coverage.out
	rm -f ./$(APP_NAME)

depend:
	go get -u ./...

vendor:
	go mod vendor

fmt:
	gofmt -w -s $$(find . -type f -name '*.go' -not -path "./vendor/*")

test: depend
	go test ./...

go-build:
	go build ./cmd/$(APP_NAME)

run: build
	./$(APP_NAME) 2>&1

docker:
	docker-compose build

start-docker: docker
	docker-compose up -d

stop-docker:
	docker-compose down

# None of the Make tasks generate files with the name of the task, so all must be declared as 'PHONY'
.PHONY: build-all clean depend vendor fmt test build run docker run-docker stop-docker
